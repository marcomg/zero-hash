# 1.0 branch #
## 1.0.3-beta ##
* Added auto selectong language support (web/)

## 1.0.2-beta ##
* Added config as arguments (script/main.php)
* Added self hash recognizer function (web/)

## 1.0.1-alpha ##
* Created web interface for searching
* Updated script for database creating

## 1.0.0-alpha ##
* Added changelog
* Added readme
* Added initdb.sql (database with null hashes)
* Added script/main.php (script which create database)

