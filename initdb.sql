CREATE TABLE IF NOT EXISTS `hashes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `string` text COLLATE utf8_unicode_ci NOT NULL,
  `md5` text COLLATE utf8_unicode_ci NOT NULL,
  `sha1` text COLLATE utf8_unicode_ci NOT NULL,
  `sha224` text COLLATE utf8_unicode_ci NOT NULL,
  `sha256` text COLLATE utf8_unicode_ci NOT NULL,
  `sha384` text COLLATE utf8_unicode_ci NOT NULL,
  `sha512` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
-- Null hashes
INSERT INTO `hashes` (`id`, `string`, `md5`, `sha1`, `sha224`, `sha256`, `sha384`, `sha512`) VALUES (NULL, '', 'd41d8cd98f00b204e9800998ecf8427e', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 'd14a028c2a3a2bc9476102bb288234c415a2b01f828ea62ac5b3e42f', 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', '38b060a751ac96384cd9327eb1b1e36a21fdb71114be07434c0cc7bf63f6e1da274edebfe76f65fbd51ad2f14898b95b', 'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e');
