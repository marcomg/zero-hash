# What is zero-hash #
zero-hash is a free script that allows you to run the brute force of a hash and save the results in a mysql database and question through a convenient web interface. The hash are: md5; sha1; sha224; sha256; sha384; sha512.

# How use zero-hash #
zero-hash is in developing so you can't use it. It'll have a script for populating the database an a web interface to search in the database.

# License #
zero-hash is licensed under the [GPLv3 license](http://www.gnu.org/licenses/gpl-3.0-standalone.html).
