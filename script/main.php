#!/usr/bin/php
<?php
##################
######CONFIG######
##################
// Password's chats
$chars = 1;

// Character set to use
$stringa_caratteri = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789`~!@#$%^&*()_-+={}[]\|:;\"'<>,.?/ ";

// Counter
$i = 1;

// Var to use in a function
$percent = null;

###########################
######CONFIG REPLACER######
###########################
if(empty($argv[1])){
	$argv[1] = 'null';
}

switch($argv[1]){
	case '-c':
		if(!empty($argv[2]) and is_numeric($argv[2])){
			$chars = $argv[2];
		}
		else{
			die('Error: argument not numeric'.PHP_EOL);
		}
	break;
	
	case 'null':
		// NULL ACTION
	break;

	default:
		die('Argument not recognized'.PHP_EOL);
	break;
}

#################
####FUNCTIONS####
#################
function hasher($string){
	$md5 = hash ('md5', $string, false);
	$sha1 = hash ('sha1', $string, false);
	$sha224 = hash ('sha224', $string, false);
	$sha256 = hash ('sha256', $string, false);
	$sha384 = hash ('sha384', $string, false);
	$sha512 = hash ('sha512', $string, false);

	// Escaping
	$string = str_replace("\\", "\\\\", $string);
	$string = str_replace("'", "\'", $string);
	
	// Query
	$query = "INSERT INTO `hashes` (`id`, `string`, `md5`, `sha1`, `sha224`, `sha256`, `sha384`, `sha512`) VALUES (null, '$string', '$md5', '$sha1', '$sha224', '$sha256', '$sha384', '$sha512');";
	
	return($query);
}


function do_perm($base,$length,$partial,&$result ){
	global $file;
	if(empty($partial)){ $partial = null; }
	//Se in $partial ci sono $length elementi lo agigungo ai risultati
		//e non faccio ninet'altro
		if( count($partial)== $length ){
		to_do(implode($partial));
		return;
		}
		//Per ogni elemento in $base

		foreach( $base as $elem ){
		//Copio la permutazione parziale corrente
		$new = $partial;
		//Aggiungo alla copia l'elemento
		$new[] = $elem;
		//Richiamo ricorsivamente la funzione per controlla se la permutazione
		// va bene (if precedente) o per aggiungere nuovi elementi
		do_perm( $base, $length, $new, $result );
	}
}

function to_do($string){
	global $i;
	global $numero_di_caratteri;
	global $chars;
	global $percent;
	global $perms;
	global $f;
	fwrite($f, hasher($string).PHP_EOL);
	$l_percent = round(($i/$perms)*100);
	if($percent!==$l_percent){
		echo('Completamento lavoro: '.$l_percent.'%'.PHP_EOL);
		$percent = $l_percent;
	}
	$i++;
}

function backup() {
	global $chars;
	$time = time();
	rename('database.tmp', $chars.'_'.$time.'_database.txt');
}

##################
######SCRIPT######
##################
while(true){
	// Preparo la stringa dei caratteri ad essere processata
	$array_caratteri = str_split($stringa_caratteri);
	$numero_di_caratteri = count($array_caratteri);
	$ultimo_dell_array = $numero_di_caratteri-1;

	// apro il database
	echo("Creazione database".PHP_EOL);
	$f = fopen('database.tmp', 'w');

	// numero di cicli da fare
	$perms = pow($numero_di_caratteri, $chars);

	// Lancio le operazioni
	echo("Inizio permutazione con $chars caratteri".PHP_EOL);
	$null = null;
	do_perm($array_caratteri, $chars, $null, $null);
	echo("Backup database in corso".PHP_EOL);
	backup();
	echo("Backup database completato".PHP_EOL);
	echo("Pulizia file temporanei".PHP_EOL);
	echo(PHP_EOL);
	$i = 1;
	$percent = null;
	$chars++;
}
?>
