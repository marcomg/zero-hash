<?php
// Set the user language if config allow it
if($config['allow_user_language']){
    $config['locale'] = get_language($config['locale']);
}
?>