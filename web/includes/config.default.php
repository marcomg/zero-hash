<?php
/*
File di configurazione php
*/
error_reporting (6143);//Quali errori stampare? 6143 tutti gli errori, 0 nessun errore

/*
File di configurazione del database. Serve per gestire il basso livello dello script
*/
define('db_hostname', 'localhost');// Indirizzo del server mysql
define('db_username', 'root');// Nome utente del server mysql
define('db_password', '');// Password del nome utente del server mysql
define('db_database', 'test');// Database mysql


/*
File di configurazione dello script php
*/
$config['db_table'] = 'hashes';

define('SCRIPT_URL', 'localhost'); // Url sito senza slash finale
define('DIR_INSTALL', '/brute/web'); // cartella nella quale è lo script Es '/cartella' (serve solo per i template) non mettere lo slash finale!! Mettere quello iniziale. Nel caso di installazione nella root non mettere nulla!
define('TEMPLATE_FOLDER', 'template'); // Url dove si trovano i template senza slash finale
define('DIR_TEMPLATE', ROOT.'/'.TEMPLATE_FOLDER);
define('DIR_TEMPLATE_VAR', './'.TEMPLATE_FOLDER.'/templates');
define('USER_CAN_REGISTER', 'true');

/*
File di configurazione di gettext
*/
define('LOCALE_DIR', ROOT .'/locale'); // Cartella dove si trovano le traduzioni
$supported_locales = array('en_US', 'it_IT'); // Lingue supportate
$encoding = 'UTF-8'; // Codifica
$config['locale'] = 'en_US'; // Lingua predefinita
$config['domain'] = 'messages'; // Dominio
$config['allow_user_language'] = true;

?>
