<?php
/*
Questo file contiene varie funzioni dello script. Sono aggiunte dall'utilizzatore
*/

/*
Function hash type return
Input hash return type
*/
function hash_type($hash){
    $s_hash = strlen($hash);
    
    switch($s_hash){
        // md5
        case 32:
            return 'md5';
        break;
        
        // sha1
        case 40:
            return 'sha1';
        break;
        
        // sha224
        case 56:
            return 'sha224';
        break;
        
        // sha256
        case 64:
            return 'sha256';
        break;
        
        // sha384
        case 96:
            return 'sha384';
        break;
        
        // sha512
        case 128:
            return 'sha512';
        break;
        
        // no results
        default:
            return false;
        break;
    }
}
?>
