<?php
// Dichiaro le costanti
define('ROOT', getcwd());

// Includo i file di configurazione e le classi
require(ROOT.'/includes/config.php');
require(ROOT.'/includes/classes/mysql.class.php');
require(ROOT.'/includes/functions/various_functions.php');
require(ROOT.'/includes/functions/script_functions.php');
require(ROOT.'/includes/classes/smarty/Smarty.class.php');
require(ROOT.'/includes/classes/gettext/gettext.inc');
require(ROOT.'/includes/commons.php');

// Dichiaro le classi
$db = new MYSQL();
$smarty = new Smarty();

// Includo il file per la configurazione di smarty
require(ROOT.'/includes/config_smarty.php');

// GETTEXT
T_setlocale(LC_MESSAGES, $config['locale']);
T_bindtextdomain($config['domain'], LOCALE_DIR);
T_textdomain($config['domain']);

// Ora avvio lo script a seconda di come è settata la variabile $_GET['action']
// Se non è settata action reindirizzo con la page settata
if(!isset($_GET['q']) or empty($_GET['q'])){
    header("location: index.php?q=pages/index");
}
else{
    $q = explode('/', $_GET['q']);
}
if(file_exists(ROOT.'/script/'.$q[0].'.php')){
    include(ROOT.'/script/'.$q[0].'.php');
}
else{
    $q[1] = 404;
    include(ROOT.'/script/errors.php');
}

?>
