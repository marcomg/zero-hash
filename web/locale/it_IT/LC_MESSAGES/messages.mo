��    $      <  5   \      0     1  @   K  +   �  e   �          5     ;     K  	   P  4   Z     �     �  	   �     �     �     �     �  	   �  -   �  	   "     ,  
   4  �   ?     �     �            5        P  &   X  
        �  v   �  2     t   @  �  �     >	  T   [	  3   �	  k   �	     P
     g
     t
     �
  
   �
  =   �
  /   �
       	             +     7     C  	   O  /   Y     �     �  
   �  �   �     [     a     u     ~  @   �  	   �  3   �  
          �   &  4   �  �   �                                                
      "                                 !      $                       	                                                        #        %1, a JavaScript library; %1, a front-end framework for faster and easier web development; %1, a library for gettext emulation in PHP; %1, a smarty plugin for using gettext in smarty templates, modified by me to use it with php-gettext. %1, a template engine; About About zero-hash Auto Error 404 Error, form is wrong: $_POST['where'] is not correct Error: no results found Hash md5 Hash sha1 Hash sha224 Hash sha256 Hash sha384 Hash sha512 Home Page It was developed by %1 and is written in PHP. Libraries License Powered by Reality is appearance, mistakes are always in wait.<br/>This page doesn't exist, now, one day it might.<br/>Come back to a safer harbour <a href="index.php">home page</a> Search Search another hash Sources String found To develop zero-hash, these libraries have been used: Welcome You'll rederict to search page&hellip; created by search hash zero-hash is a software wich allows you to know the origin world of hashes: md5; sha1; sha224; sha256; sha384; sha512. zero-hash is licensed under the %1GPLv3%2 license. zero-hash sources are freely available from our <a href="http://bitbucket.org/marcomg/zero-hash">git repository</a>. Project-Id-Version: 1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-04-29 17:53+0200
PO-Revision-Date: 2013-04-29 17:54+0200
Last-Translator: Marco Guerrini <marcomg@cryptolab.net>
Language-Team: Italiano <>
Language: Italian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 2.91.5
 %1, una libreria JavaScript; %1, un framework per l'interfaccia grafica per un più veloce e facile sviluppo web; %1, una libreria per l'emulazione di gettext in PHP %1, un plugin per smarty per usare gettext nei template smarty, modificato da me per usarlo con php-gettext %1, un template engine Informazioni Informazioni su zero-hash Auto Errore 404 Errore, il form è sbagliato: $_POST['where'] non è corretto Errore: la ricerca non ha riscontrato risultati Hash md5 Hash sha1 Hash sha224 Hash sha256 Hash sha384 Hash sha512 Home Page È stato sviluppato da %1 ed è scritto in PHP. Librerie Licenza Powered by La realtà è apparenza, l'errore è sempre in agguato.<br/>Questa pagina ora non esiste, ma un giorno forse, chissà.<br/>Torna ad un porto sicuro: <a href="index.php">home page</a Cerca Cerca un altro hash Sorgenti Stringa trovata Per sviluppare zero-hash, sono state utilizzate queste librerie: Benvenuto Sarete reindirizzati alla pagina di ricerca&hellip; created by cerca un hash zero-hash è un software che ti permette di sapere la parola originale che ha generato gli hash: md5; sha1; sha224; sha256; sha384; sha512. zero-hash è disponibile sotto la %1licenza GPLv3%2. I sorgenti di zero-hash sources sono liberamente disponibili dal nostro <a href="http://bitbucket.org/marcomg/zero-hash">repository git</a>. 