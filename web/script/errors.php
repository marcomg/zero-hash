<?php
if(empty($q[1])){
    $q[1] = '?';
}

switch($q[1]){
    case '404':
        header("Status: 404 Not Found");
        $smarty->assign('title', T_('Error 404'));
        $smarty->display('error404.tpl');
    break;
    
    default:
        header('Location: index.php');
    break;
}
?>
