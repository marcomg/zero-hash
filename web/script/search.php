<?php
// If not isset or empty print form
if(!isset($_POST['hash']) or !isset($_POST['where']) or empty($_POST['hash']) or empty($_POST['where'])){
    $smarty->assign('title', T_('Search'));
    $smarty->display('search_form.tpl');
}
// If form ok
else{
    $hash = $db->escape_string($_POST['hash']);
    $where = $db->escape_string($_POST['where']);
    
    // If where is auto i set where as true type
    if($where == 'auto'){
        $where = hash_type($hash);
    }
    
    if($hash == 'oblique'){
        $smarty->assign('esteregg_oblique', true);
    }
    
    // Set query ad where
    switch($where){
        case 'md5':
            $query = "SELECT * FROM `{$config['db_table']}` WHERE `md5` = '{$_POST['hash']}'";
        break;
        
        case 'sha1':
            $query = "SELECT * FROM `{$config['db_table']}` WHERE `sha1` = '{$_POST['hash']}'";
        break;
        
        case 'sha224':
            $query = "SELECT * FROM `{$config['db_table']}` WHERE `sha224` = '{$_POST['hash']}'";
        break;
        
        case 'sha256':
            $query = "SELECT * FROM `{$config['db_table']}` WHERE `sha256` = '{$_POST['hash']}'";
        break;
        
        case 'sha384':
            $query = "SELECT * FROM `{$config['db_table']}` WHERE `sha384` = '{$_POST['hash']}'";
        break;
        
        case 'sha512':
            $query = "SELECT * FROM `{$config['db_table']}` WHERE `sha512` = '{$_POST['hash']}'";
        break;
        
        default:
            $smarty->assign('title', T_('Search'));
            $smarty->assign('error', T_('Error, form is wrong: $_POST[\'where\'] is not correct'));
            $smarty->display('search_form.tpl');
            exit;
        break;
        
    }
    // Excute query
    $result = $db->query($query);
    
    // Take results
    $i = 0; // Set counter
    while($_ = $db->fetch_array($result)){
        $i++;
        $id[] = $_['id'];
        $string[] = $_['string'];
        $md5[] = $_['md5'];
        $sha1[] = $_['sha1'];
        $sha224[] = $_['sha224'];
        $sha256[] = $_['sha256'];
        $sha384[] = $_['sha384'];
        $sha512[] = $_['sha512'];
    }
    unset($_, $query, $result); // Unset temp vars
    
    // If zero results
    if($i == 0){
        $smarty->assign('error', T_('Error: no results found'));
        $smarty->assign('title', T_('Search'));
        $smarty->display('search_form.tpl');
    }
    // If one or more results
    else{
        $smarty->assign('title', T_('Search'));
        $smarty->assign('id', $id);
        $smarty->assign('string', $string);
        $smarty->assign('md5', $md5);
        $smarty->assign('sha1', $sha1);
        $smarty->assign('sha224', $sha224);
        $smarty->assign('sha256', $sha256);
        $smarty->assign('sha384', $sha384);
        $smarty->assign('sha512', $sha512);
        
        $smarty->display('search_result.tpl');
    }
    
}

?>
