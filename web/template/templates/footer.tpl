        <br/>
        <div class="container">
            <div class="text-center">
                <p><small class="muted">{t}Powered by{/t} <a href="https://bitbucket.org/marcomg/zero-hash">zero-hash</a> {t}created by{/t} <a href="http://www.marcomg.tk/">marcomg</a></small></p>
            </div>
        </div>
    </body>
</html>
