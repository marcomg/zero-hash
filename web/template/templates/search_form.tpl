{include file='header.tpl'}
        <div class="container">
            <div class="text-center">
                
                <p class="lead"><img src="{$smarty.const.DIR_TEMPLATE_VAR}/images/favicon.ico"/> {t}search hash{/t}</p>
                
                <form class="form-horizontal" action="index.php?q=search" method="post">
                    <input type="text" class="input-medium search-query" name="hash">
                    <select class="span1" name="where">
                        <option>md5</option>
                        <option>sha1</option>
                        <option>sha224</option>
                        <option>sha256</option>
                        <option>sha384</option>
                        <option>sha512</option>
                        <option value="auto">{t}Auto{/t}</option>
                    </select>
                    <label></label>
                    <button type="submit" class="btn"><i class="icon-search"></i> {t}Search{/t}</button>
                </form>
            </div>
        </div>
{include file='footer.tpl'}