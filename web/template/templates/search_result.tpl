{include file='header.tpl'}
        <div class="container">
            <p><a class="btn" href="">{t}Search another hash{/t}</a></p>
{section name=i loop=$id}
            <p style="word-wrap:break-word;">
                {t}String found{/t}: <b>{$string[i]|escape:'htmlall'}</b><br/>
                {t}Hash md5{/t}: <b>{$md5[i]}</b><br/>
                {t}Hash sha1{/t}: <b>{$sha1[i]}</b><br/>
                {t}Hash sha224{/t}: <b>{$sha224[i]}</b><br/>
                {t}Hash sha256{/t}: <b>{$sha256[i]}</b><br/>
                {t}Hash sha384{/t}: <b>{$sha384[i]}</b><br/>
                {t}Hash sha512{/t}: <b>{$sha512[i]}</b><br/>
            </p>
{/section}
        </div>
{include file='footer.tpl'}